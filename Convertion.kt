class Convertion {
    private var t: Int = 0
    private var varVide: Int = 0
    private var ligneGlobale: Int = 0
    private var ligne: Int = 0
    private var colonne: Int = 0
    private var ligneTest: Int = 0
    private var colonneTest: Int = 0
    private var tab: Array<Array<String>>? = null
    private var ftab: Array<Array<String>>? = null
    private var fn: Array<String>? = null

    fun testEpsilonTransition(a: AFN) {
        if (a.testEpsilon() == true)
            t = 0 //epsilon-transition
        else
            t = 1
    }

    fun TableTransition() {
        var i: Int
        var j: Int
        i = 0
        while (i < ftab!!.size) {
            j = 0
            while (j < ftab!![0].size) {
                print(ftab!![i][j] + "\t \t")
                j++
            }
            println()
            i++
        }
    }

    fun afficherAFD(a: AFD, b: AFN) {
        var i: Int
        val taille = b.Alphabet.size()
        i = 0
        while (i < taille) {
            a.Alphabet.add(b.Alphabet.get(i))
            i++
        }
        var k = 1
        do {
            a.Etat.add(ftab!![k][0])
            k++
        } while (k < ftab!!.size)
        a.EtatInitial = ftab!![2][0]
        for (m in 0 until b.EtatsFinaux.size()) {
            val et = b.EtatsFinaux.get(m)
            for (n in 2 until ftab!!.size) {
                if (ftab!![n][0].contains(et) == true) a.EtatsFinaux.add(ftab!![n][0])
            }
        }
        a.FctTransition = Array(a.Etat.size()) { arrayOfNulls(a.Alphabet.size()) }
        var po = 0
        var pi: Int
        for (mp in 1 until ftab!!.size) {
            pi = 0
            for (mo in 1 until ftab!![mp].size) {
                a.FctTransition[po][pi] = ftab!![mp][mo]
                pi++
            }
            po++
        }
    }

    fun toAfd(n: AFN) {
        varVide = 0
        var temp: ArrayList<String>
        run {
            var e = 0
            while (e < n.Etat.size()) {
                var a = 0
                while (a < n.Alphabet.size()) {
                    if (n.FctTransition[e][a] === "Vide")
                        varVide = 1
                    a = n.Alphabet.size()
                    a++
                }
                e = n.Etat.size()
                e++
            }
        }
        if (t == 0) {

            tab = Array(100) { arrayOfNulls(n.Alphabet.size() + 1) }

            for (i in 0..99) {
                for (j in 0 until n.Alphabet.size() + 1) {
                    tab!![i][j] = "ftab"
                }
            }
            tab!![0][0] = " "
            for (i in 1..n.Alphabet.size())
                tab!![0][i] = n.Alphabet.get(i - 1).toString()
            for (i in 0 until n.Alphabet.size() + 1)
                tab!![1][i] = "vide"
            tab!![2][0] = "{s0}"
            ligneTest = 2
            colonneTest = 1
            ligne = 2
            colonne = 1
            for (j in 0 until n.Alphabet.size()) {
                tab!![ligne][colonne] = n.FctTransition[0][j]
                colonne++
            }
            ligne++
            colonne = 0
            ligneGlobale = 3
            remplirligne(n.Alphabet.size(), n)
            while (tab!![ligneGlobale][0] !== "ftab") {
                temp = ArrayList()
                for (l in 0 until n.Etat.size()) {
                    val c = n.Etat.get(l)
                    if (tab!![ligneGlobale][0].contains(c) == true) {
                        temp.add(c)
                    }
                }

                remplircolonne(temp, n)
                remplirligne(n.Alphabet.size(), n)
            }


            ftab = Array(ligneGlobale) { arrayOfNulls(n.Alphabet.size() + 1) }
            for (j in 0 until ligneGlobale) {
                for (k in 0 until n.Alphabet.size() + 1) {
                    ftab!![j][k] = tab!![j][k]
                }
            }

        } else {
            fn = arrayOfNulls(n.Etat.size())
            for (i in 0 until n.Etat.size()) {
                val e = n.Etat.get(i)
                fn[i] = e
                if (n.FctTransition[i][n.Alphabet.size()] !== "vide") {
                    val z = n.FctTransition[i][n.Alphabet.size()]
                    fn[i] += z
                }
            }

            for (j in 0 until n.Etat.size()) {
                for (l in 0 until n.Etat.size()) {
                    if (l != j) {
                        if (fn!![l].contains(n.Etat.get(j))) {
                            fn[l] += fn!![j]
                        }

                    }
                }
            }
            var et = 0
            do {

                val t = ArrayList<String>()
                for (l in 0 until n.Etat.size()) {
                    val c = n.Etat.get(l)
                    if (fn!![et].contains(c) == true) {
                        t.add(c)
                    }
                }
                var chaine = "{"
                for (l in 0 until t.size) {
                    chaine = chaine + t[l]
                    if (l != t.size - 1)
                        chaine = chaine + ","
                    else
                        chaine = chaine + "}"
                }

                fn[et] = chaine
                et++
            } while (et < n.Etat.size)

            tab = Array(100) { arrayOfNulls(n.Alphabet.size() + 1) }
            for (i in 0..99) {
                tab!![i][0] = "ftab"
            }
            tab!![0][0] = " "
            for (z in 1..n.Alphabet.size)
                tab!![0][z] = n.Alphabet.get(z - 1).toString()
            if (varVide == 1 || varVide == 0) {
                for (i in 0..n.Alphabet.size)
                    tab!![1][i] = "vide"
            }
            tab!![2][0] = fn!![0]
            ligneTest = 2
            colonneTest = 1
            ligne = 2
            colonne = 1
            ligneGlobale = 2

            while (tab!![ligneGlobale][0] !== "ftab") {


                temp = ArrayList()

                for (l in 0 until n.Etat.size) {
                    val c = n.Etat.get(l)

                    if (tab!![ligneGlobale][0].contains(c) == true) {
                        temp.add(c)
                    }
                }
                remplircolonne(temp, n)
                if (ligneGlobale == 3) ligne++
                remplirligne(n.Alphabet.size(), n)
                if (tab!![ligneGlobale][0] === "ftab") {
                    break
                }
            }
            ftab = Array(ligneGlobale) { arrayOfNulls(n.Alphabet.size + 1) }
            for (j in 0 until ligneGlobale) {
                for (k in 0 until n.Alphabet.size + 1) {
                    ftab!![j][k] = tab!![j][k]
                }
            }
        }

    }

    fun remplircolonne(tem: ArrayList<String>, af: AFN) {
        var tp: ArrayList<String>
        colonne = 1
        var j: Int
        var i: Int
        if (tem.size === 1 && t == 0) {
            j = 0
            while (j < af.Alphabet.size) {
                val a = tem[0].substring(1, 2)
                val z = Integer.parseInt(a)
                tab!![ligneGlobale][colonne] = af.FctTransition[z][j]
                colonne++
                j++
            }
            ligneGlobale++
            colonne = 0
        } else {

            j = 0
            while (j < af.Alphabet.size) {
                i = 0
                while (i < tem.size) {
                    val a = tem[i].substring(1, 2)
                    val z = Integer.parseInt(a)
                    if (i == 0) {
                        tab!![ligneGlobale][colonne] = af.FctTransition[z][j]
                    } else {
                        tab!![ligneGlobale][colonne] += af.FctTransition[z][j]
                    }
                    i++
                }
                var testVide = true
                run {
                    var l = 0
                    while (l < af.Etat.size) {
                        val c = af.Etat.get(l)
                        if (tab!![ligneGlobale][colonne].contains(c) == true) {
                            testVide = false
                            l = af.Etat.size
                        }
                        l++
                    }
                }
                if (testVide == true) tab!![ligneGlobale][colonne] = "vide"
                if (tab!![ligneGlobale][colonne] !== "vide") {
                    if (t == 1) {
                        for (l in 0 until af.Etat.size) {
                            val c = af.Etat.get(l)
                            if (tab!![ligneGlobale][colonne].contains(c) == true) {
                                tab!![ligneGlobale][colonne] += fn!![l]

                            }
                        }
                    }

                    tp = ArrayList()
                    for (l in 0 until af.Etat.size) {
                        val c = af.Etat.get(l)
                        if (tab!![ligneGlobale][colonne].contains(c) == true) {
                            tp.add(c)
                        }
                    }
                    var chaine = "{"
                    for (l in 0 until tp.size) {
                        chaine = chaine + tp[l]
                        if (l != tp.size - 1)
                            chaine = chaine + ","
                        else
                            chaine = chaine + "}"
                    }
                    tab!![ligneGlobale][colonne] = chaine
                    colonne++
                }
                j++
            }

            ligneGlobale++
            colonne = 0
        }
    }

    fun remplirligne(taille: Int, af: AFN) {
        var tailleGlobaleParcours = 1
        var test: Boolean
        do {
            test = true

            var m = 2
            while (m < ligne) {
                val a = tab!![ligneTest][colonneTest]
                val b = tab!![m][0]
                if (a == b == true && a.length == b.length) {
                    test = false
                    m = ligne
                }
                m++

            }

            if (test == true) {
                tab!![ligne][0] = tab!![ligneTest][colonneTest]
                ligne++
            }

            if (colonneTest == taille) {
                ligneTest++
                colonneTest = 1
            } else {
                colonneTest++
            }
            tailleGlobaleParcours++
        } while (tailleGlobaleParcours <= taille)
    }
}
