import javax.swing.*;
import java.awt.*;

public class Interface {

    public static void main(String[] args) {

        JPanel f= new JPanel();
        f.setBackground(Color.PINK);

        JLabel label1 = new JLabel("APPLICATION : Convertir AFN à AFD");
        Font fontTitre = new Font("Arial",Font.BOLD,32);
        Font fontOptions = new Font("Arial",Font.ITALIC,12);
        label1.setFont(fontTitre);
        JLabel label2 = new JLabel("Gestion des symboles : ");
        label2.setFont(fontOptions);
        JLabel label3 = new JLabel("Gestion des états :");
        label3.setFont(fontOptions);
        JButton convertir = new JButton("Convertir");
        JButton ajouter1 = new JButton("Ajouter symbole");
        JButton ajouter2 = new JButton("Ajouter état");

        f.add(label1);
        f.add(label2);
        f.add(ajouter1);
        f.add(label3);
        f.add(ajouter2);
        f.add(convertir);

        JFrame frame = new JFrame(
                "My First Calculator");
        frame.setContentPane(f);

        frame.setSize(600,600);
        frame.setResizable(false);
        frame.setVisible(true); }

}
