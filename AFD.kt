package ApplAplhabet

import java.util.*

class AFD() : AFN() {

    override lateinit var Etat: ArrayList<String>
    override lateinit var Alphabet: ArrayList<Char>
    override lateinit var EtatsFinaux: ArrayList<String>
    override lateinit var FctTransition: Array<Array<String>>

    override internal var sc = Scanner(System.`in`)

    init {
        Etat = ArrayList()
        Alphabet = ArrayList()
        EtatsFinaux = ArrayList()
    }

    override fun afficherAlphabet() {
        val size = Alphabet.size
        for (i in 0 until size) {
            print(Alphabet[i] + "\t\t")
        }
    }

    override fun afficherEtat() {
        val taille = Etat.size
        for (i in 0 until taille) {
            print(Etat[i] + ",")
        }
        println()
    }

    override fun afficherEtatsFinaux() {
        val size = EtatsFinaux.size
        for (i in 0 until size) {
            println(EtatsFinaux[i] + " ")
        }
        println()
    }

    override fun afficherEtatInitial() {
        println("L'état initial est : S0")
    }

    override fun remplirFctTransition() {
        var etatTest: String
        var f: String
        var c: Char
        var i: Int
        var j: Int
        i = 0
        while (i < Etat.size) {
            etatTest = Etat[i]
            j = 0
            while (j < Alphabet.size) {
                c = Alphabet[j]
                print("f$etatTest,$c)=")
                f = sc.nextLine()
                FctTransition[i][j] = f
                j++
            }
            i++

        }
    }

    override fun afficherFctTransition() {
        print("\t\t")
        afficherAlphabet()
        println()
        for (i in Etat.indices) {
            print(Etat[i] + "\t\t")
            for (j in Alphabet.indices) {
                print(FctTransition[i][j] + "\t\t")
            }
            println()
        }
    }
}