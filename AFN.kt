package ApplAplhabet

import java.util.*

open class AFN {

    open lateinit var Etat: ArrayList<String>
    open lateinit var Alphabet: ArrayList<Char>
    open lateinit var EtatsFinaux: ArrayList<String>
    var etatInitial: String=""
    open lateinit var FctTransition: Array<Array<String>>

    open internal var sc = Scanner(System.`in`)

    init {
        Etat = ArrayList()
        Alphabet = ArrayList()
        EtatsFinaux = ArrayList()
    }

    //***************** MENU ETAT *************************

    fun menuEtat() {
        println("1 : Ajouter Etat\n2 : Afficher Etat \n3 : Quitter")
    }

    fun ajouterEtat() {
        val n: Int
        var e: String
        println("Entrer Nombres d'Etats : ")
        n = sc.nextInt()
        for (i in 0 until n) {
            e = "S" + i
            Etat.add(e)
        }
    }

    open fun afficherEtat() {
        val taille = Etat.size
        for (i in 0 until taille - 1) {
            print(Etat[i] + ",")
        }
        print(Etat[taille - 1])
        println()
    }

    //***************** MENU ALPHABET *************************

    fun menuAlphabet() {
        println("1 : Ajouter Alphabet\n2 : Afficher Alphabet \n3 : Quitter")
    }

    fun ajouterAlphabet() {
        val s: Char
        val i: Int
        println("Entrer le Caractère que vous voulez ajouter : ")
        s = sc.next().get(0)

        if (Alphabet.size != 0) {
            i = Alphabet.indexOf(s)
            if (i != -1) {
                println("ERROR ! Ce symbole existe déja !")
            } else {
                Alphabet.add(s)
                println("Votre symbole a été ajouté avec succès.\nLe symbole ajouté est : $s")
            }
        } else {
            Alphabet.add(s)
            println("Votre symbole a été ajouté avec succès.\nLe symbole ajouté est : $s")
        }
    }

    open fun afficherAlphabet() {
        val size = Alphabet.size
        for (i in 0 until size) {
            print(Alphabet[i] + "\t\t")
        }
    }

    //***************** MENU ETATS INITIAL *************************

    open fun afficherEtatInitial() {
        println("L'état initial est : S0")
    }

    //***************** MENU ETATS FINAUX *************************

    fun saisirEtatsFinaux() {
        var f: String
        val n: Int
        println("Entrer le nombre d'états finaux : ")
        n = sc.nextInt()
        for (i in 0 until n) {
            print("Entrer l'état final N°" + (i + 1) + " : S")
            f = sc.next()
            EtatsFinaux.add(f)
        }
    }

    open fun afficherEtatsFinaux() {
        val size = EtatsFinaux.size
        for (i in 0 until size) {
            println("S" + EtatsFinaux[i] + " ")
        }
        println()
    }

    //***************** FONCTION TRANSITION *************************

    open fun remplirFctTransition() {
        var `var`: String
        var tab: ArrayList<String>
        var r: Int
        var y: Int
        for (i in Etat.indices) {
            val s = Etat[i]
            for (j in 0 until Alphabet.size + 1) {
                if (j == Alphabet.size) {
                    println("Entrer le résultat de ($s,Epsilon) : ")
                    println("CHOIX : \n1 : Vide\n2 : Un ou Plusieus états\nVotre choix est : ")
                } else {
                    val a = Alphabet[j]
                    println("Entrer le résultat de ($s,$a) : ")
                    println("CHOIX : \n1 : Vide\n2 : Un ou Plusieus états\nVotre choix est : ")
                }
                var rep = sc.nextInt()
                while (rep != 1 && rep != 2) {
                    println("ERROR ! Veuillez entrer l'un des choix proposés : ")
                    rep = sc.nextInt()
                }
                if (rep == 1) {
                    FctTransition[i][j] = "Vide"
                }
                if (rep == 2) {
                    tab = ArrayList()
                    println("Veuillez saisir le nombre d'états : ")
                    r = sc.nextInt()
                    //System.out.println("Veuillez saisir le nombre d'états");
                    var l: Int
                    l = 1
                    while (l <= r) {
                        print("Donner l'état $l : S")
                        do {
                            `var` = "S" + sc.nextLine()
                            y = Etat.indexOf(`var`)
                            //y=tab.indexOf(var);
                        } while (y == -1)
                        tab.add(`var`)
                        l++
                    }

                    var chaine = "{"
                    l = 0
                    while (l < tab.size) {
                        if (l < tab.size - 1) {
                            chaine += tab[l] + ","
                        } else {
                            chaine += tab[l] + "}"
                        }
                        l++
                    }
                    FctTransition[i][j] = chaine
                }
            }
        }
    }

    open fun afficherFctTransition() {
        print("\t\t")
        afficherAlphabet()
        println("Epsilon")
        for (i in Etat.indices) {
            print(Etat[i] + "\t\t")

            for (j in 0 until Alphabet.size + 1) {
                print(FctTransition[i][j] + "\t\t")
            }
            println()
        }
    }

    fun testEpsilon(): Boolean {
        var test = true
        var i: Int
        val j: Int
        j = Alphabet.size
        i = 0
        while (i < Etat.size) {
            if (FctTransition[i][j] !== "Vide") {
                test = false
                i = Etat.size
            }
            i++
        }
        return test
    }
}